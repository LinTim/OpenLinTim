"""
File contains all needed constants for net file reading
"""
EDGE_SECTION_HEADER = "$STRECKE"
EDGE_NAME_HEADER = "NR"
EDGE_FROM_NAME_HEADER = "VONKNOTNR"
EDGE_TO_NAME_HEADER = "NACHKNOTNR"
LINE_SECTION_HEADER = "$LINIE"
LINE_NAME_HEADER = "NAME"
LINE_VSYSCODE_HEADER = "VSYSCODE"
LINE_VEHICLE_COMBINATION_NUMBER_HEADER = "FZGKOMBNR"
LINE_ROUTE_SECTION_HEADER = "$LINIENROUTENELEMENT"
LINE_ROUTE_NAME_HEADER = "LINNAME"
LINE_ROUTE_DIRECTION_HEADER = "RICHTUNGCODE"
LINE_ROUTE_NODE_HEADER = "KNOTNR"
LINE_TRAVERSAL_SECTION_HEADER = "$FAHRPLANFAHRT"
LINE_TRAVERSAL_TIME_HEADER = "ABFAHRT"
LINE_TRAVERSAL_LINE_NAME_HEADER = "LINNAME"
LINE_TIME_TRAVERSAL_SECTION_HEADER = "$FAHRZEITPROFILELEMENT"
LINE_TIME_TRAVERSAL_LINE_NAME_HEADER = "LINNAME"
LINE_TIME_TRAVERSAL_DIRECTION_HEADER = "RICHTUNGCODE"
LINE_TIME_TRAVERSAL_STOP_HEADER = "INDEX"
LINE_TIME_TRAVERSAL_ARRIVAL_HEADER = "ANKUNFT"
LINE_TIME_TRAVERSAL_DEPARTURE_HEADER = "ABFAHRT"
VEHICLE_UNITS_SECTION_HEADER = "$FZGEINHEIT"
VEHICLE_UNITS_NUMBER_HEADER = "NR"
VEHICLE_UNITS_CAPACITY_HEADER = "GESAMTPL"
VEHICLE_UNITS_COMBINATION_HEADER = "$FZGEINHEITZUFZGKOMB"
VEHICLE_COMBINATION_NUMBER_HEADER = "FZGKOMBNR"
VEHICLE_COMBINATION_UNITS_HEADER = "FZGEINHEITNR"

NON_FIXED_VSYSCODE = "B"
