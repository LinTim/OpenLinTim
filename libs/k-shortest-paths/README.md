# K-shortest-paths

## Version

d028fd8

## Filename

*.java

## Project Homepage

https://github.com/yan-qi/k-shortest-paths-java-version

## Needed by

- Line pool generation (/src/linepool)

## Remarks

Just copy every .java file in this folder and remove the package annotations