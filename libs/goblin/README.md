# Goblin

## Version

1.0

## Filename

- include/*
- lib/libgoblin.a
- lib/libgoblin_32.a

## Project Homepage

http://goblin2.sourceforge.net/

## Needed by

- aperiodic network simplex (/src/timetabling/aperiodic/networksimplex)
- modulo simplex (/src/timetabling/periodic/modulo-simplex)