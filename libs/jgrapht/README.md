# Jgrapht

## Version

1.1.0

## Filename

jgrapht-core-1.1.0.jar

## Project Homepage

https://jgrapht.org/

## Needed by

- ptn load generator (/src/tools/ptn-load-generator)
- shortest path essentials library (/src/essentials/shortest-paths)
- javatools (/src/essentials/javatools)